# Public DMT Downloads
Private and Confidential.  Copyright (c) 2017 Project C.U.R.E.

Serves the downloadable public DMT mobile apps (build artifacts) from the [DMT](https://bitbucket.org/nordy4projectcure/dmt/) project to allow end-users to install/update their apps.

This is an *empty* repository, except for this single README file and the downloadable files in the Downloads area.